﻿#include <iostream>
using namespace std;
#include <string>

struct Bag
{
	string books[];
};

struct Student
{
	int Age = 0;
	int Height = 0;
	string Name = 0;
	Bag* myBag = nullptr;

	void GetInfo()
	{
		cout << "student struct";
	}
};
int main()
{	
	Student* ptr = new Student{ 20, 180, "Paul" };
	ptr->GetInfo();
}


